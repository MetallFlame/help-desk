package mycompany.mapper;

import mycompany.dto.UserDTO;
import mycompany.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class UserMapperTest {

    @InjectMocks
    private UserMapper userMapper;

    @Mock
    private ModelMapper mapper;

    private User entity;

    private UserDTO dto;

    @BeforeEach
    public void setup() {
        entity = new User();
        entity.setEmail("user@email.com");
        entity.setFirstName("userFirstname");
        entity.setId(1L);


        dto = new UserDTO();
        dto.setEmail("user@email.com");
        dto.setFirstName("userFirstname");
        dto.setId(1L);

        Mockito.lenient().when(mapper.map(entity, UserDTO.class)).thenReturn(dto);
        Mockito.lenient().when(mapper.map(dto, User.class)).thenReturn(entity);
    }

    @Test
    public void toDTOTest() {
        assertEquals(dto.getClass(), userMapper.toDTO(entity).getClass());
        assertEquals(dto.getEmail(), userMapper.toDTO(entity).getEmail());
        assertEquals(dto.getFirstName(), userMapper.toDTO(entity).getFirstName());
        assertEquals(dto.getId(), userMapper.toDTO(entity).getId());
    }

    @Test
    public void toEntityTest() {
        assertEquals(entity.getClass(), userMapper.toEntity(dto).getClass());
        assertEquals(entity.getEmail(), userMapper.toEntity(dto).getEmail());
        assertEquals(entity.getFirstName(), userMapper.toEntity(dto).getFirstName());
        assertEquals(entity.getId(), userMapper.toEntity(dto).getId());
    }
}
