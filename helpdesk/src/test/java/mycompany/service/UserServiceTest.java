package mycompany.service;

import mycompany.dto.UserDTO;
import mycompany.exception.UserNotFoundException;
import mycompany.model.UserRole;
import mycompany.mapper.UserMapper;
import mycompany.model.User;
import mycompany.repository.UserRepository;
import mycompany.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    private List<User> users;
    private List<UserDTO> usersDTO;

    private User user1;
    private User user2;

    private UserDTO user1DTO;
    private UserDTO user2DTO;

    @BeforeEach
    public void setup() throws Exception {
        users = new ArrayList<>();
        usersDTO = new ArrayList<>();

        user1 = new User();
        user1.setEmail("user1@email.com");
        user1.setId(1L);
        user1.setFirstName("user1FirstName");
        user1.setLastName("user1LastName");
        user1.setRole(UserRole.EMPLOYEE);

        user2 = new User();
        user2.setEmail("user2@email.com");
        user2.setId(2L);
        user2.setFirstName("user2FirstName");
        user2.setLastName("user2LastName");
        user2.setRole(UserRole.MANAGER);

        user1DTO = new UserDTO();
        user1DTO.setEmail("user1@email.com");
        user1DTO.setId(1L);
        user1DTO.setFirstName("user1FirstName");
        user1DTO.setLastName("user1LastName");
        user1DTO.setRole(UserRole.EMPLOYEE);

        user2DTO = new UserDTO();
        user2DTO.setEmail("user2@email.com");
        user2DTO.setId(2L);
        user2DTO.setFirstName("user1FirstName");
        user2DTO.setLastName("user1LastName");
        user2DTO.setRole(UserRole.MANAGER);

        users.add(user1);
        users.add(user2);

        usersDTO.add(user1DTO);
        usersDTO.add(user2DTO);

        Mockito.lenient().when(userRepository.getById(1L)).thenReturn(java.util.Optional.ofNullable(user1));
        Mockito.lenient().when(userRepository.getById(2L)).thenReturn(java.util.Optional.ofNullable(user2));

        Mockito.lenient().when(userMapper.toDTO(user1)).thenReturn(user1DTO);
        Mockito.lenient().when(userMapper.toDTO(user2)).thenReturn(user2DTO);
        Mockito.lenient().when(userMapper.toDTOList(users)).thenReturn(usersDTO);
    }

    @Test
    public void getAllTest() {
        Mockito.when(userRepository.getAll()).thenReturn(users);
        assertEquals(usersDTO, userService.getAll());
    }

    @Test
    public void getByIdTest() throws UserNotFoundException {
        assertEquals(user1DTO, userService.getDTOById(1L));
        assertEquals(user2DTO, userService.getDTOById(2L));
    }

    @Test
    public void isRegisteredTest() throws UserNotFoundException {
        Mockito.when(userRepository.getByEmailAndPassword(user1DTO.getEmail(), user1DTO.getPassword())).thenReturn(java.util.Optional.ofNullable(user1));
        assertEquals(user1DTO, userService.getUserFromDBWithData(user1DTO));
    }

    @Test
    public void getEmailTest() throws UserNotFoundException {
        assertEquals("user1@email.com", userService.getDTOById(1l).getEmail());
        assertEquals("user2@email.com", userService.getDTOById(2l).getEmail());
    }

    @Test
    public void getUserRoleTest() throws UserNotFoundException {
        assertEquals(UserRole.EMPLOYEE, userService.getDTOById(1L).getRole());
        assertEquals(UserRole.MANAGER, userService.getDTOById(2L).getRole());
    }

    @Test
    public void getAllManagersEmailsTest() {
        Mockito.when(userRepository.getAllByRole(UserRole.MANAGER)).thenReturn(users);
        assertEquals("user1@email.com, user2@email.com", userService.getAllManagersEmails());
    }

    @Test
    public void getAllEngineersEmailsTest() {
        Mockito.when(userRepository.getAllByRole(UserRole.ENGINEER)).thenReturn(users);
        assertEquals("user1@email.com, user2@email.com", userService.getAllEngineersEmails());
    }
}
