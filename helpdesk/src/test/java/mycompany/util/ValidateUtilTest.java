package mycompany.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class ValidateUtilTest {

//    @Autowired
//    ValidateUtil validateUtil;

    @InjectMocks
    private ValidateUtil validateUtil;

    @Test
    public void userEmailValidateTest() {
        assertTrue(validateUtil.userEmailValidate("test@gmail.com"));
        assertFalse(validateUtil.userEmailValidate("test@gmail"));
        assertFalse(validateUtil.userEmailValidate("gmail.com"));
    }

    @Test
    public void userPasswordValidateTest() {
        assertTrue(validateUtil.userPasswordValidate("12345Aa$"));
        assertFalse(validateUtil.userPasswordValidate("12345Aa"));
        assertFalse(validateUtil.userPasswordValidate("12345&"));
    }

    @Test
    public void ticketNameValidateTest() {
        assertTrue(validateUtil.ticketNameValidate("testName"));
        assertFalse(validateUtil.ticketNameValidate("№123"));
    }

    @Test
    public void ticketDescriptionValidateTest() {
        assertTrue(validateUtil.ticketDescriptionValidate("test description"));
        assertFalse(validateUtil.ticketDescriptionValidate("№123"));
    }

    @Test
    public void ticketDesiredResolutionDateValidateTest() throws ParseException {
        assertTrue(validateUtil.ticketDesiredResolutionDateValidate(LocalDateTime.of(2022, 1, 12, 1, 1, 1)));
        assertTrue(validateUtil.ticketDesiredResolutionDateValidate(LocalDateTime.of(2020, 1, 12, 1, 1, 1)));
    }

    @Test
    public void commentTextValidateTest() {
        assertTrue(validateUtil.commentTextValidate("test comment"));
        assertFalse(validateUtil.commentTextValidate("№ 12345"));
    }

    @Test
    public void feedbackRateValidateTest() {
        assertTrue(validateUtil.feedbackRateValidate(4L));
        assertFalse(validateUtil.feedbackRateValidate(7L));
    }

    @Test
    public void feedbackTextValidateTest() {
        assertTrue(validateUtil.feedbackTextValidate("This is valid feedback"));
        assertFalse(validateUtil.feedbackTextValidate("This is invalid feedback, because it's contains '№' symbol"));
    }

}
