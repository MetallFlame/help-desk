package mycompany.service;

import mycompany.model.Category;

import java.util.List;

public interface CategoryService {
    List<String> getAllCategories();

    Category getByName(String categoryName);
}
