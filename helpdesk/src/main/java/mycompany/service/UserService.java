package mycompany.service;

import mycompany.dto.UserDTO;
import mycompany.model.User;

import java.util.List;

public interface UserService {
    List<UserDTO> getAll();

    UserDTO getDTOById(Long userId);

    UserDTO getDTOByEmail(String email);

    User getById(Long userId);

    User getByEmail(String email);

    UserDTO getUserFromDBWithData(UserDTO user);

    String getAllManagersEmails();

    String getAllEngineersEmails();
}
