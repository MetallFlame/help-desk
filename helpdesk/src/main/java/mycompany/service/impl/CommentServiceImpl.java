package mycompany.service.impl;

import mycompany.dto.CommentDTO;
import mycompany.mapper.CommentMapper;
import mycompany.model.Comment;
import mycompany.repository.CommentRepository;
import mycompany.service.CommentService;
import mycompany.service.TicketService;
import mycompany.service.UserService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Transactional
@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final UserService userService;
    private final TicketService ticketService;
    private final CommentMapper commentMapper;

    public CommentServiceImpl(CommentRepository commentRepository,
                              CommentMapper commentMapper,
                              UserService userService,
                              TicketService ticketService) {
        this.commentRepository = commentRepository;
        this.commentMapper = commentMapper;
        this.userService = userService;
        this.ticketService = ticketService;
    }

    @Override
    public List<CommentDTO> getByTicketId(Long ticketId) {
        return commentMapper.toDTOList(commentRepository.getByTicketId(ticketId));
    }

    @Override
    public void createComment(Long ticketId, Long userId, String commentText) {
        Comment newComment = new Comment();
        newComment.setUser(userService.getById(userId));
        newComment.setDate(LocalDateTime.now());
        newComment.setTicket(ticketService.getById(ticketId));
        newComment.setText(commentText);
        commentRepository.createComment(newComment);
    }
}
