package mycompany.service.impl;

import mycompany.dto.FeedbackDTO;
import mycompany.exception.FeedbackNotFoundException;
import mycompany.mapper.FeedbackMapper;
import mycompany.model.Feedback;
import mycompany.repository.FeedbackRepository;
import mycompany.service.FeedbackService;
import mycompany.service.TicketService;
import mycompany.service.UserService;
import mycompany.util.EmailUtil;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;
    private final FeedbackMapper feedbackMapper;
    private final TicketService ticketService;
    private final UserService userService;
    private final EmailUtil emailUtil;

    FeedbackServiceImpl(FeedbackRepository feedbackRepository,
                        FeedbackMapper feedbackMapper,
                        TicketService ticketService,
                        UserService userService,
                        EmailUtil emailUtil) {
        this.feedbackRepository = feedbackRepository;
        this.feedbackMapper = feedbackMapper;
        this.emailUtil = emailUtil;
        this.ticketService = ticketService;
        this.userService = userService;
    }

    @Override
    public FeedbackDTO getByTicketId(Long ticketId) {
        return feedbackMapper.toDTO(feedbackRepository.getByTicketId(ticketId).orElseThrow(FeedbackNotFoundException::new));
    }

    @Override
    public void createFeedback(FeedbackDTO feedbackDTO, Long ticketId, Long userId) {
        Feedback newFeedback = feedbackMapper.toEntity(feedbackDTO);
        newFeedback.setTicket(ticketService.getById(ticketId));
        newFeedback.setUser(userService.getById(userId));
        feedbackRepository.createFeedback(newFeedback);
        emailUtil.sendEmailForFeedback(newFeedback.getTicket().getAsignee().getId(), newFeedback.getTicket().getId());
    }
}
