package mycompany.service.impl;

import mycompany.exception.CategoryNotFoundException;
import mycompany.model.Category;
import mycompany.repository.CategoryRepository;
import mycompany.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<String> getAllCategories() {
        return categoryRepository.getAll().stream().map(category -> category.getName()).collect(Collectors.toList());
    }

    @Override
    public Category getByName(String categoryName) {
        return categoryRepository.getByName(categoryName).orElseThrow(CategoryNotFoundException::new);
    }
}
