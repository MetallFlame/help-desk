package mycompany.service;

import mycompany.dto.FeedbackDTO;

public interface FeedbackService {
    FeedbackDTO getByTicketId(Long ticketId);

    void createFeedback(FeedbackDTO feedbackDTO, Long ticketId, Long userId);
}
