package mycompany.mapper;


import mycompany.dto.HistoryDTO;
import mycompany.model.History;
import mycompany.model.User;
import mycompany.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class HistoryMapper {

    private ModelMapper mapper;
    private UserRepository userRepository;

    @Autowired
    public HistoryMapper(ModelMapper mapper, UserRepository userRepository) {
        this.mapper = mapper;
        this.userRepository = userRepository;
    }

    public HistoryDTO toDTO(History entity) {
        if (Objects.isNull(entity)) {
            return null;
        } else {
            User historyUser = entity.getUser();
            entity.setUser(null);
            HistoryDTO dto = mapper.map(entity, HistoryDTO.class);
            dto.setUserName(historyUser.getFirstName() + " " + historyUser.getLastName());
            return dto;
        }
    }

    public List<HistoryDTO> toDTOList(List<History> histories) {
        return histories.stream().map(history -> toDTO(history)).collect(Collectors.toList());
    }
}
