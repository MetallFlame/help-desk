package mycompany.mapper;


import mycompany.dto.CommentDTO;
import mycompany.exception.UserNotFoundException;
import mycompany.model.Comment;
import mycompany.model.User;
import mycompany.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class CommentMapper {

    private ModelMapper mapper;
    private UserService userService;

    @Autowired
    public CommentMapper(ModelMapper mapper, UserService userService) {
        this.mapper = mapper;
        this.userService = userService;
    }

    public CommentDTO toDTO(Comment entity) throws UserNotFoundException {
        if (Objects.isNull(entity)) {
            return null;
        } else {
            User commentator = userService.getById(entity.getUser().getId());
            entity.setUser(null);
            CommentDTO dto = mapper.map(entity, CommentDTO.class);
            dto.setUserName(commentator.getFirstName() + " " + commentator.getLastName());
            return dto;
        }
    }

    public List<CommentDTO> toDTOList(List<Comment> comments) throws UserNotFoundException {
        List<CommentDTO> commentsDTO = new ArrayList<>();

        for (Comment comment : comments) {
            commentsDTO.add(toDTO(comment));
        }

        return commentsDTO;
    }
}
