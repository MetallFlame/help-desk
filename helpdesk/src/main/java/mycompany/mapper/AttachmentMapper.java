package mycompany.mapper;

import mycompany.dto.AttachmentDTO;
import mycompany.model.Attachment;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class AttachmentMapper {

    private ModelMapper mapper;

    @Autowired
    public AttachmentMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public AttachmentDTO toDTO(Attachment entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, AttachmentDTO.class);
    }

    public List<AttachmentDTO> toDTOList(List<Attachment> attachments) {
        return attachments.stream().map(attachment -> toDTO(attachment)).collect(Collectors.toList());
    }
}
