package mycompany.repository;

import mycompany.model.Feedback;

import java.util.Optional;

public interface FeedbackRepository {
    void createFeedback(Feedback newFreedback);

    Optional<Feedback> getByTicketId(Long ticketId);

    Optional<Feedback> getById(Long feedbackId);
}
