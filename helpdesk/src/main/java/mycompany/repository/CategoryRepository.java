package mycompany.repository;

import mycompany.model.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository {
    List<Category> getAll();

    Optional<Category> getByName(String categoryName);
}
