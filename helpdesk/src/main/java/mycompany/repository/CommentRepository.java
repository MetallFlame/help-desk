package mycompany.repository;

import mycompany.model.Comment;

import java.util.List;

public interface CommentRepository {
    List<Comment> getByTicketId(Long ticketId);

    void createComment(Comment newComment);
}
