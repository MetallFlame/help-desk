package mycompany.repository;

import mycompany.model.History;

import java.util.List;

public interface HistoryRepository {
    List<History> getByTicketId(Long ticketId);

    void addHistory(History historyString);
}
