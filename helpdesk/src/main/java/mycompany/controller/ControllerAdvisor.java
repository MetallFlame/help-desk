package mycompany.controller;

import mycompany.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor {

    private final static String TIMESTAMP = "timestamp";
    private final static String MESSAGE = "message";
    private final static String ACTION = "action";
    private final static String ATTACHMENT = "attachment";
    private final static String CATEGORY = "category";
    private final static String FEEDBACK = "feedback";
    private final static String TICKET = "ticket";
    private final static String USER = "user";
    private final static String TICKET_CHOISE = "ticket choise";
    private final static String NOT_FOUND = " not found";
    private final static String NOT_ACCEPTABLE = " not acceptable";



    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Object> handleUserNotFoundException () {
        return prepareResponseForSubject(USER + NOT_FOUND, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(TicketNotFoundException.class)
    public ResponseEntity<Object> handleTicketNotFoundException () {
        return prepareResponseForSubject(TICKET + NOT_FOUND, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ActionNotFoundEcxeption.class)
    public ResponseEntity<Object> handleActionNotFoundException () {
        return prepareResponseForSubject(ACTION + NOT_FOUND, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AttachmentNotFoundException.class)
    public ResponseEntity<Object> handleAttachmentNotFoundException () {
        return prepareResponseForSubject(ATTACHMENT + NOT_FOUND, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CategoryNotFoundException.class)
    public ResponseEntity<Object> handleCategoryNotFoundException () {
        return prepareResponseForSubject(CATEGORY + NOT_FOUND, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(FeedbackNotFoundException.class)
    public ResponseEntity<Object> handleFeedbackNotFoundException () {
        return prepareResponseForSubject(FEEDBACK + NOT_FOUND, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(WrongTicketChoiseEcxeption.class)
    public ResponseEntity<Object> handleWrongTicketChoiseException () {
        return prepareResponseForSubject(TICKET_CHOISE + NOT_ACCEPTABLE, HttpStatus.NOT_ACCEPTABLE);
    }

    private ResponseEntity<Object> prepareResponseForSubject(String responseSubject, HttpStatus responseStatus) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put(TIMESTAMP, LocalDateTime.now());
        body.put(MESSAGE, responseSubject);
        return new ResponseEntity<>(body, responseStatus);
    }

}
