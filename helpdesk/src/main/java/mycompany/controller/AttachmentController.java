package mycompany.controller;

import mycompany.dto.AttachmentDTO;
import mycompany.service.AttachmentService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/attachments")
public class AttachmentController {

    private final AttachmentService attachmentService;

    public AttachmentController(AttachmentService attachmentService) {
        this.attachmentService = attachmentService;
    }

    @GetMapping("")
    public ResponseEntity<List<AttachmentDTO>> getAttachmentDTOListByTicketId(@RequestParam Long ticketId) {
        return ResponseEntity.ok(attachmentService.getAllByTicketId(ticketId));
    }

    @PostMapping(path = "", consumes = "multipart/form-data")
    public ResponseEntity<Map> uploadAttachment(@RequestParam("attachment") MultipartFile multipartFile,
                                                @RequestParam Long ticketId,
                                                @RequestParam Long userId) throws IOException {
        attachmentService.createAttachment(multipartFile.getBytes(),
                multipartFile.getOriginalFilename(), ticketId, userId);
        return ResponseEntity.ok(Collections.singletonMap("Response", "Attachment created"));
    }

    @GetMapping("/{attachmentId}")
    public ByteArrayResource downloadAttachment(@PathVariable Long attachmentId) {
        byte[] attachment = attachmentService.getContentByAttachmentId(attachmentId);
        return new ByteArrayResource(attachment);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map> deleteAttachment(@PathVariable Long id,
                                                @RequestParam Long ticketId,
                                                @RequestParam Long userId) {
        attachmentService.deleteAttachment(id, ticketId, userId);
        return ResponseEntity.ok(Collections.singletonMap("Response", "Attachment deleted"));
    }
}
