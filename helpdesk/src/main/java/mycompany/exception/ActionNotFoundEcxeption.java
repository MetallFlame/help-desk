package mycompany.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

public class ActionNotFoundEcxeption extends RuntimeException {

    private static final Logger logger = LoggerFactory.getLogger(ActionNotFoundEcxeption.class);

    public ActionNotFoundEcxeption() {
        logger.error(LocalDateTime.now() + ": action not found");
    }
}
