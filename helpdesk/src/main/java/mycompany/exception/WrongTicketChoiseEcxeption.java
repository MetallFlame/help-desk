package mycompany.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

public class WrongTicketChoiseEcxeption extends RuntimeException {

    private static final Logger logger = LoggerFactory.getLogger(WrongTicketChoiseEcxeption.class);

    public WrongTicketChoiseEcxeption() {
        logger.error(LocalDateTime.now() + ": wrong ticket choise");
    }
}
