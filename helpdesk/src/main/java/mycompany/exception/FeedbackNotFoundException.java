package mycompany.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

public class FeedbackNotFoundException extends RuntimeException {

    private static final Logger logger = LoggerFactory.getLogger(FeedbackNotFoundException.class);

    public FeedbackNotFoundException() {
        logger.error(LocalDateTime.now() + ": feedback not found");
    }
}
