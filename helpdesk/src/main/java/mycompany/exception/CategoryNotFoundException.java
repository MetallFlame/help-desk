package mycompany.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

public class CategoryNotFoundException extends RuntimeException {

    private static final Logger logger = LoggerFactory.getLogger(CategoryNotFoundException.class);

    public CategoryNotFoundException() {
        logger.error(LocalDateTime.now() + ": category not found");
    }
}
