package mycompany.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

public class TicketNotFoundException extends RuntimeException {

    private static final Logger logger = LoggerFactory.getLogger(TicketNotFoundException.class);

    public TicketNotFoundException() {
        logger.error(LocalDateTime.now() + ": ticket not found");
    }
}
