package mycompany.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

public class AttachmentNotFoundException extends RuntimeException {

    private static final Logger logger = LoggerFactory.getLogger(AttachmentNotFoundException.class);

    public AttachmentNotFoundException() {
        logger.error(LocalDateTime.now() + ": attachment not found");
    }
}
