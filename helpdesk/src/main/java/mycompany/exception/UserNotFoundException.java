package mycompany.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

public class UserNotFoundException extends RuntimeException {

    private static final Logger logger = LoggerFactory.getLogger(UserNotFoundException.class);

    public UserNotFoundException() {
        logger.error(LocalDateTime.now() + ": user not found");
    }
}
