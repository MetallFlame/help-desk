package mycompany.model;

public enum UserRole {
    EMPLOYEE,
    MANAGER,
    ENGINEER
}
