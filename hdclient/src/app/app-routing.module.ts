import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
import { PageComponent } from './page/page.component';
import { LoginComponent } from './component/login/login.component';
import { TicketsComponent } from './component/tickets/tickets.component';
import { CreateComponent } from './component/create/create.component';
import { OverviewComponent } from './component/overview/overview.component';
import { FeedbackComponent } from './component/feedback/feedback.component';
import { EditComponent } from './component/edit/edit.component';
import { CommentFormComponent } from './component/commentform/commentform.component';
import { AttachmentComponent } from './component/attachment/attachment.component';
 
const appRoutes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'tickets', component: TicketsComponent},
  {path: 'create', component: CreateComponent},
  {path: 'overview', component: OverviewComponent},
  {path: 'feedback', component: FeedbackComponent},
  {path: 'edit', component: EditComponent},
  {path: 'commentform', component: CommentFormComponent},
  {path: 'attachment', component: AttachmentComponent},
  {path: '**', redirectTo: '/home', pathMatch: 'full'}
];
 
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
}) 
export class AppRoutingModule {
}