import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule }   from '@angular/common/http';

import { AppComponent } from './app.component';
import { PageComponent } from './page/page.component';
import { LoginComponent } from './component/login/login.component';
import { TicketsComponent } from './component/tickets/tickets.component';
import { CreateComponent } from './component/create/create.component';
import { OverviewComponent } from './component/overview/overview.component';
import { EditComponent } from './component/edit/edit.component';
import { FeedbackComponent } from './component/feedback/feedback.component';
import { MenuComponent } from './component/menu/menu.component';
import { CommentFormComponent } from './component/commentform/commentform.component';
import { AttachmentComponent } from './component/attachment/attachment.component';



@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    LoginComponent,
    TicketsComponent,
    CreateComponent,
    OverviewComponent,
    EditComponent,
    FeedbackComponent,
    MenuComponent,
    CommentFormComponent,
    AttachmentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
