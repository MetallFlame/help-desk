export class Comment{
    id: number;
    text: string;
    userName: string;
    date: Date;
} 