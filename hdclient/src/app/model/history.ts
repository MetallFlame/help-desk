export class History{
    id: number;
    date: Date;
    action: string;
    description: string;
    userName: String;
}