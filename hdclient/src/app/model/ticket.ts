
import {Urgency} from './urgency';

export class Ticket {
    id: number;
    name: string;
    description: string;
    createdOn: Date;
    desiredResolutionDate: Date;
    state: string;
    category: string;
    urgency: Urgency;
    ownerString: string;
    asigneeString: string;
    approverString: string;
}