export class Feedback{
    id: number;
    text: string;
    rate: number;
}