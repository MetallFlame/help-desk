import { Component, OnInit } from '@angular/core';
import { identifierModuleUrl } from '@angular/compiler';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/model/user';
import { Routes, RouterModule, Router } from '@angular/router';
import { SubscriptionLike } from 'rxjs';
import { CookieStorage } from 'cookie-storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: any = {
    login:  '',
    password: ''
  }

  loginWarning: string = '';
  passwordWarning: string = '';

  
  subscription: SubscriptionLike;


  constructor(private http: HttpClient, private router: Router) {

   }


  ngOnInit(): void {
    const cookieStorage = new CookieStorage();
    console.log(window.localStorage.getItem('userId'));
    if ((localStorage.getItem('userId') != undefined)||(cookieStorage.getItem('userId') != undefined)) {
      this.router.navigate(['/tickets']);
    } 
  }

  ngOnDestroy() {
  }

  sendForm(){
    let loginFlag = this.loginValidate();
    let passwordFlag = this.passwordValidate();


    if (loginFlag&&passwordFlag) {
        this.loginRequest();
      
    }
  }

  loginRequest() {
    const request  = {email: this.loginForm.login, password: this.loginForm.password};
    const cookieStorage = new CookieStorage();
    this.subscription = this.http.post('http://localhost:8080/api/v1/users', request).subscribe((data: User) => {
      if (data.id>0) {
        //переход на главную страницу
        window.localStorage.setItem('userId', data.id.toString());
        window.localStorage.setItem('userLogin', data.email);
        window.localStorage.setItem('userRole', data.role.toString());
        cookieStorage.setItem('userId', data.id.toString());
        cookieStorage.setItem('userLogin', data.email);
        cookieStorage.setItem('userRole', data.role.toString());
        this.subscription.unsubscribe();
        this.router.navigate(['/tickets']);
      } else {
        //перезагрузка страницы
        this.loginForm.login="";
        this.loginForm.password="";
        //this.router.navigate(['/login']);
        this.passwordWarning="incorrect login or password";
        this.subscription.unsubscribe();        
     }
    });
  }

  loginValidate(): boolean {
    let regexp: RegExp = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    if (this.loginForm.login == ''){
      this.loginWarning = 'fill login field';
      return false;
    }

    if(!regexp.test(this.loginForm.login)||(this.loginForm.login.length > 100)) {
      this.loginWarning = 'enter valid email';
      return false;
    }

    this.loginWarning = '';
    return true;
  }

  passwordValidate(): boolean {
    let uppercaseRegexp: RegExp = /[A-Z]/;
    let lowercaseRegexp: RegExp = /[a-z]/;
    let symbolsRegexp: RegExp = /[\~\.\"\(\)\,\:\;\<\>\@\[\]\!\#\$\%\&\'\*\+-\/\=\?\^\_\`\{\|\}]/;
    if (this.loginForm.password == ''){
      this.passwordWarning = 'fill password field';
      return false;
    }

    if ((this.loginForm.password.length < 6)||
    (this.loginForm.password.length > 20)||(
    (!uppercaseRegexp.test(this.loginForm.password))&&
    (!lowercaseRegexp.test(this.loginForm.password))&&
    (!symbolsRegexp.test(this.loginForm.password)))) {
      this.passwordWarning = 'enter valid password';
      return false;
    }

    this.passwordWarning = '';
    return true;
  }
 

}
