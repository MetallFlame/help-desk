import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { SubscriptionLike } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Attachment } from 'src/app/model/attachment';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss']
})
export class AttachmentComponent implements OnInit {

  attachmentList: Attachment[];
  uploadAttachmentSubscription: SubscriptionLike;
  attachmentListSubscription: SubscriptionLike;
  deleteAttachmentSubscription: SubscriptionLike;
  downloadAttachmentSubscription: SubscriptionLike;
  selectedFile: File = null;
  ticketForAttachId: string;



  constructor(private router: Router, private http: HttpClient, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    if  (localStorage.getItem('userId') == undefined) {
      this.router.navigate(['/login']);
    }

    if (localStorage.getItem('ticketForAttachId') == undefined) {
      this.goToTickets();
    }

    this.ticketForAttachId = localStorage.getItem('ticketForAttachId');
    this.attachmentListRequest();
  }

  ngOnDestroy(): void {

    localStorage.removeItem('ticketForAttachId');
  }

  goToTickets() {
    this.router.navigate(['/tickets']);
  }

  uploadFiles() {
    
  }

  onFileSelected(event) {
    this.selectedFile = <File>event.target.files[0];
}

  onUpload() {
    const myHeaders = new HttpHeaders().set('Content-Type', 'multipart/form-data');

    const fd = new FormData();

    const request = {name: this.selectedFile.name, content: this.selectedFile};

    fd.append('attachment', this.selectedFile, this.selectedFile.name);

    this.uploadAttachmentSubscription = this.http.post('http://localhost:8080/api/ticket/attachment/upload/1', fd, {
      headers: {
        'enctype': 'multipart/form-data; boundary=request-boundary',
        'Accept': 'application/json'
      }
    })
        .subscribe((data: number) => {
            console.log('res: ', data);
            this.uploadAttachmentSubscription.unsubscribe();
            this.attachmentListRequest();
            this.selectedFile = null;
        });
  }

  attachmentListRequest() {
    this.attachmentListSubscription = this.http.get('http://localhost:8080/api/ticket/attachment/getall/1').subscribe((data: Attachment[]) => {
      this.attachmentList = data;
      this.attachmentListSubscription.unsubscribe();
    });
  }

  deleteAttachment(attachmentId: number): void {
    this.deleteAttachmentSubscription = this.http.delete('http://localhost:8080/api/ticket/attachment/delete/' + attachmentId.toString()).subscribe((data: number) => {
      this.deleteAttachmentSubscription.unsubscribe();
      this.attachmentListRequest();
    });
  }

  downloadAttachment(attachmentId: string,  filename: string): void {
    this.downloadAttachmentSubscription = this.http.get('http://localhost:8080/api/ticket/attachment/download/' + attachmentId,{responseType: 'blob' as 'json'}).subscribe(
        (response: any) =>{
            const blob = new Blob([response], { type: 'application/octet-stream' });
            let downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(blob);
            document.body.appendChild(downloadLink);
            downloadLink.setAttribute('download', filename);
            downloadLink.click();
        }
    )
  }

}
