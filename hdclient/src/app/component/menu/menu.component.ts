import { Component, OnDestroy, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { CookieStorage } from 'cookie-storage';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {

  userLogin: string = null;

  constructor(private router: Router) { }

  ngOnDestroy(): void {
    
  }

  ngOnInit(): void {
    this.userLogin = localStorage.getItem('userLogin');
  }

  navigate(adress: String) {
    this.router.navigate([adress]);
  }

  
  logout() {
    const cookieStorage = new CookieStorage();
    cookieStorage.clear();
    localStorage.clear();
    this.router.navigate(['/login']);

  }

}
