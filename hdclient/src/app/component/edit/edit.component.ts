import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { SubscriptionLike } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Ticket } from 'src/app/model/ticket';
import { Urgency } from 'src/app/model/urgency';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})

export class EditComponent implements OnInit {

  editForm: Ticket;
  editedTicketId: String;
  categorySubscription: SubscriptionLike;
  editSubscription: SubscriptionLike;
  createCommentSubscription: SubscriptionLike;
  ticketUrgency = '';
  urgency = Urgency;
  categoryList: String[];
  createInputWarningFlag: boolean = false;
  isDescriptionCorrectFlag: boolean = true;
  isNameCorrectFlag: boolean = true;
  createSelectWarningFlag: boolean = false;
  isCommentCorrectFlag: boolean = true;
  commentText: string = '';
  createCommentWarningFlag: boolean = false;
  

  dateMin = Date.now();

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {

    if  (localStorage.getItem('userId') == undefined) {
      this.router.navigate(['/login']);
    }

    if (localStorage.getItem('editedTicketId') == undefined) {
      this.router.navigate(['/tickets']);
    }

    this.editedTicketId = localStorage.getItem("editedTicketId");
    this.ticketRequest(this.editedTicketId);
    this.getCategories();
  }

  ngOnDestroy(): void {
    localStorage.removeItem("editedTicketId");
  }

  ticketRequest(ticketId: String) {
    this.editSubscription = this.http.get('http://localhost:8080/api/v1/tickets/' + ticketId).subscribe((data: Ticket) => {
      this.editForm = data;
      this.ticketUrgency = data.urgency.toString().toLowerCase();
      this.ticketUrgency = this.ticketUrgency[0].toLocaleUpperCase() + this.ticketUrgency.substr(1);
      console.log(this.ticketUrgency);
      this.editSubscription.unsubscribe();
    });
  }

  goToOverview() {
    this.router.navigate(['/overview'], { queryParams: { id: this.editedTicketId.toString() }, queryParamsHandling: 'merge' });
  }

  getCategories() {
    this.categorySubscription = this.http.get('http://localhost:8080/api/v1/categories').subscribe((data: String[]) => {
      this.categoryList = data;
      this.categorySubscription.unsubscribe();
    });
  }

  editTicket() {
    if (this.isNameCorrect()&&
    this.isDescriptionCorrect()&&
    this.isCategorySelected()&&
    this.isUrgencySelected()&&
    this.isNameinputted()&&
    this.isCommentCorrect()) {
      var desiredDate: string = this.editForm.desiredResolutionDate.toString();
      const request = {name: this.editForm.name, 
        description: this.editForm.description,
         category: this.editForm.category, 
         urgency: this.ticketUrgency.toUpperCase( )}
       
      this.editSubscription = this.http.post('http://localhost:8080/api/ticket/edit/' + this.editedTicketId + '/' + localStorage.getItem('userId') + '/' 
      +  desiredDate.substring(0, 4) + '/'
      + desiredDate.substring(5, 7) + '/'
      + desiredDate.substring(8), request).subscribe((data: number) => {
        if (data == 1) {
          console.log('Edit ticket connection passed!');
        }
        if (this.commentText != '') {
          this.commentCreateRequest(this.editedTicketId);
        }
        this.editSubscription.unsubscribe();
        this.goToOverview();
      });
    }
    if (!this.isDescriptionCorrect()||!this.isNameCorrect()) {
      this.createInputWarningFlag = true;
    }

    if (!this.isNameinputted()||!this.isUrgencySelected()||!this.isCategorySelected()) {
      this.createSelectWarningFlag = true;
    }

    if (!this.isCommentCorrect()) {
      this.createCommentWarningFlag = true;
    }
  }

  stateNormalize(state: string): string {
    state = state.toLowerCase();
    return  (state[0].toLocaleUpperCase() + state.substr(1)).replace("_", " ");
  }

  regexpCheck(stringForCheck: string): boolean {
    let regexp: RegExp = /[^0-9a-zA-Z\~\.\s\"\(\)\,\:\;\<\>\@\[\]\!\#\$\%\&\'\*\+-\/\=\?\^\_\`\{\|\}]/;
    if (regexp.test(stringForCheck)) {
    return false;
    }
    return true;
  }

  isNameCorrect(): boolean {
    if (this.regexpCheck(this.editForm.name)&&this.editForm.name.length < 100) {
      return true;
    }
    return false;
  }

  isDescriptionCorrect(): boolean {
    if (this.regexpCheck(this.editForm.description)&&this.editForm.description.length < 500) {
      return true;
    }
    return false;
  }

  isCategorySelected(): boolean {
    if (this.editForm.category != '') { 
      this.createSelectWarningFlag = false;
      return true;
    }
    return false;
  }

  isUrgencySelected(): boolean {
    if (this.ticketUrgency != '') {
      this.createSelectWarningFlag = false;
      return true;
    }
    return false;
  }

  isNameinputted(): boolean {
    if (this.editForm.name != '') {
      this.createSelectWarningFlag = false;
      return true;
    }
    return false;
  }

  onNameChange() {
    this.createInputWarningFlag = false;
    if (this.isNameCorrect()) {
      this.isNameCorrectFlag = true;
    } else {
      this.isNameCorrectFlag = false;
    }
  }

  onDescriptionChange() {
    this.createInputWarningFlag = false;
    if (this.isDescriptionCorrect()) {
      this.isDescriptionCorrectFlag = true;
    } else {
      this.isDescriptionCorrectFlag = false;
    }
  }

  onCommentChange() {
      this.createCommentWarningFlag = false;
      if (this.isCommentCorrect()) {
        this.isCommentCorrectFlag = true;
      } else {
        this.isCommentCorrectFlag = false;
      }
  }

  isCommentCorrect(): boolean {
    if (this.regexpCheck(this.commentText)&&this.commentText.length < 500) {
      return true;
    }
    return false;
  }

  commentCreateRequest(ticketId: String) {
    const request = {text: this.commentText, userId: localStorage.getItem('userId')};
    this.createCommentSubscription = this.http.post('http://localhost:8080/api/v1/comments?ticketId=' + ticketId, request).subscribe((data: number) => {
        this.createCommentSubscription.unsubscribe();
      });
  }



}
