import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SubscriptionLike } from 'rxjs';
import { Ticket } from 'src/app/model/ticket';
import { CookieStorage } from 'cookie-storage';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit {

  ticketSubscription: SubscriptionLike;
  ticketList: Ticket[];
  reserveTicketList: Ticket[];
  userRole: string;
  previousSortedColumn: string = "";
  filterOption: string = "";
  filterCriteria: string = "";
  columnCheck: string = "urgency";
  filterCriteriaWarning: string = "";
  sortDirection: string;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    const cookieStorage = new CookieStorage();
    if  ((localStorage.getItem('userId') == undefined)&&(cookieStorage.getItem('userId') == undefined)) {
      this.router.navigate(['/login']);
    }

    if (localStorage.getItem('userId') == undefined) {
      localStorage.setItem('userId', cookieStorage.getItem('userId'));
    }

    if (localStorage.getItem('userRole') == undefined) {
      localStorage.setItem('userRole', cookieStorage.getItem('userRole'));
    }

    if (localStorage.getItem('userlogin') == undefined) {
      localStorage.setItem('userLogin', cookieStorage.getItem('userLogin'));
    }

    this.userRole = localStorage.getItem('userRole');
    if (this.userRole == 'EMPLOYEE') {
      this.ticketsRequest('own');
    } else {
          this.ticketsRequest('relevant');
    }


  }

  ngOnDestroy() {
  }

  ticketsRequest(selection: String) {
    this.ticketSubscription = this.http.get('http://localhost:8080/api/v1/tickets?selection=' + selection + '&userId=' + localStorage.getItem('userId')).subscribe((data: Ticket[]) => {
      this.ticketList = data;

      var i: number;
      for(i = 0; i < this.ticketList.length; i++) {
        this.ticketList[i].desiredResolutionDate = this.returnDate(this.ticketList[i].desiredResolutionDate);
      }

      console.log(data);
      this.reserveTicketList = this.ticketList;
      this.sort('urgency');
      this.ticketSubscription.unsubscribe();
    });
  }

  editTicket(ticketId: number) {
    localStorage.setItem("editTicketId", ticketId.toString());
    this.router.navigate(['/edit']);
  }

  overviewTicket(ticketId: number) {
    this.router.navigate(['/overview'], { queryParams: { id: ticketId.toString() }, queryParamsHandling: 'merge' });
  }


  userRoleCheck(rolefromHtml: String): boolean {
    if(rolefromHtml == localStorage.getItem('userRole')) {
      return true;
    }
      return false;
  }


  isRightAction(action: String, ticket: Ticket) :boolean {
      let userRole = localStorage.getItem('userRole');
      if (userRole == 'EMPLOYEE') {
        if (((ticket.state == 'DRAFT')||(ticket.state == 'DECLINED'))&&((action == 'submit')||(action == 'cancel'))) {
          return true;
        }
      }

      if (userRole == 'MANAGER') {
        if (((ticket.state == 'DRAFT')||(ticket.state == 'DECLINED'))&&((action == 'submit')||(action == 'cancel'))) {
          
          return true;
        }

        if ((ticket.state == 'NEW')&&((action == 'approve')||(action == 'decline')||(action == 'cancel'))) {
         
          return true;
        }
      }

      if (userRole == 'ENGINEER') {
        if ((ticket.state == 'APPROVED')&&((action == 'asign')||(action == 'cancel'))) {
          return true;
        }
        if((ticket.state == "IN_PROGRESS")&&(action == "done")) {
          return true;
        }
        
      }
      return false;


  }

  doAction(ticket: Ticket, action: String) {
     console.log(action + ' ' + ticket.id);
      this.ticketSubscription = this.
      http.
      patch('http://localhost:8080/api/tickets/' + ticket.id + '?action='+ action + '&userId='  + localStorage.getItem('userId'), null)
      .subscribe(
        data => {
            this.ticketSubscription.unsubscribe();
            if (localStorage.getItem('userRole') == 'EMPLOYEE') {
            this.ticketsRequest('user');
            } else {
              this.ticketsRequest('all');
            } 
        }
      );
  }

  goToCreate() {
    this.router.navigate(['/create']);
  }

  createComment(ticketId: number) {
    localStorage.setItem('commentedTicketId', ticketId.toString())
    this.router.navigate(['/commentform']);
  }

  sort(colName) {
    if (colName != 'urgency') {
    if (this.previousSortedColumn != colName) {
    this.ticketList.sort((a, b) => a[colName] > b[colName] ? 1 : a[colName] < b[colName] ? -1 : this.sortByDesiredDate(a, b));
    this.previousSortedColumn = colName;
    this.columnCheck = colName;
    this.sortDirection = "&#9660;";
    } else {
      this.ticketList.sort((a, b) => a[colName] < b[colName] ? 1 : a[colName] > b[colName] ? -1 : this.sortByDesiredDate(a, b));
      this.previousSortedColumn = "";
      this.columnCheck = colName;
      this.sortDirection = "&#9650;";
    }
  } else {
    if (this.previousSortedColumn == colName) {
      this.ticketList.sort((a, b) => {
          if (a.urgency.toString() == "CRITICAL") {
              if (b.urgency.toString() == "CRITICAL") {
              return this.sortByDesiredDate(a, b);
            }
            return 1;
          }

          if (a.urgency.toString() == "HIGH") {
            if (b.urgency.toString() == "CRITICAL") {
              return -1;
            }
            if (b.urgency.toString() == "HIGH") {
              return this.sortByDesiredDate(a, b);
            }
            return 1;
          }

          if (a.urgency.toString() == "MEDIUM") {
            if (b.urgency.toString() == "MEDIUM") {
              return this.sortByDesiredDate(a, b);
            }
            if (b.urgency.toString() == "LOW") {
              return 1;
            }
            return -1;
          }

          if (a.urgency.toString() == "LOW") {
            if (b.urgency.toString() == "LOW") {
              return this.sortByDesiredDate(a, b);
            }
            return -1;
          }
      });
      this.previousSortedColumn = "";
      this.columnCheck = colName;
      this.sortDirection = "&#9650;";
      } else {
        this.ticketList.sort((a, b) => {
          if (b.urgency.toString() == "CRITICAL") {
            if (a.urgency.toString() == "CRITICAL") {
            return this.sortByDesiredDate(a, b);
          }
          return 1;
        }

        if (b.urgency.toString() == "HIGH") {
          if (a.urgency.toString() == "CRITICAL") {
            return -1;
          }
          if (a.urgency.toString() == "HIGH") {
            return this.sortByDesiredDate(a, b);
          }
          return 1;
        }

        if (b.urgency.toString() == "MEDIUM") {
          if (a.urgency.toString() == "MEDIUM") {
            return this.sortByDesiredDate(a, b);
          }
          if (a.urgency.toString() == "LOW") {
            return 1;
          }
          return -1;
        }

        if (b.urgency.toString() == "LOW") {
          if (a.urgency.toString() == "LOW") {
            return this.sortByDesiredDate(a, b);
          }
          return -1;
        }
      });

        this.previousSortedColumn = colName;
        this.columnCheck = colName;
        this.sortDirection = " &#9660;";
      }
  }
}

sortByDesiredDate(a, b): number {
  return a['desiredResolutionDate'] > b['desiredResolutionDate'] ? 1 : a['desiredResolutionDate'] < b['desiredResolutionDate'] ? -1 : 0;
}

  doFilter() {
    
    if (this.filterOption != "") {
      if (this.filterCriteria != "") {
        if (this.filterCriteriaValidate()) {
          this.ticketList = this.reserveTicketList;
          this.ticketList = this.ticketList.filter(a => a[this.filterOption].toString().toLowerCase().indexOf(this.filterCriteria.toLowerCase())!=-1);
          this.filterCriteriaWarning = "";
        } else {
          this.filterCriteriaWarning = "please, enter valid filter criteria";
        }
      } else {
        this.ticketList = this.reserveTicketList;
        this.filterCriteriaWarning = "";
      }
    }
  }

  filterCriteriaValidate(): boolean {
    let regexp: RegExp = /[^0-9a-zA-Z\~\.\"\(\)\,\:\;\<\>\@\[\]\!\#\$\%\&\'\*\+-\/\=\?\^\_\`\{\|\}]/;
    if(regexp.test(this.filterCriteria)) {
      return false;
    } else {
      return true;
    }
  }

  stateNormalize(state: string): string {
    state = state.toLowerCase();
    return  (state[0].toLocaleUpperCase() + state.substr(1)).replace("_", " ");
  }

  returnDate(localDate: any): Date {
    var date = new Date();

    date.setFullYear(localDate.year);
    date.setMonth(localDate.monthValue);
    date.setDate(localDate.dayOfMonth);
    date.setHours(localDate.hour);
    date.setMinutes(localDate.minute);
    date.setSeconds(localDate.second);

    return date;
  }

 
}
