import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { Subscription, SubscriptionLike } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
    selector: 'app-commentform',
    templateUrl: './commentform.component.html',
    styleUrls: ['./commentform.component.scss']
}

)

export class CommentFormComponent implements OnInit {

    commentText: String;
    commentedTicketId: String;
    commentSubscription: SubscriptionLike;
    
    constructor(private http: HttpClient, private router: Router, ) { }

    ngOnInit(): void {
        if  (localStorage.getItem('commentedTicketId') == undefined) {
            this.router.navigate(['/login']);
          }
    }

    createComment() {
        const request = {text: this.commentText, userId: localStorage.getItem('userId')};
        this.commentSubscription = this.http.post('http://localhost:8080/api/ticket/createcomment/' + localStorage.getItem('commentedTicketId'), request).subscribe((data: number) => {
            if (data == 1) {
              console.log('connection test passed');
            }
            this.commentSubscription.unsubscribe();
            this.goToOverview();
          });

    }

    ngOnDestroy(): void {
        localStorage.removeItem('commentedTicketId');
    }

    goToOverview() {
        localStorage.setItem('overviewTicketId', localStorage.getItem('commentedTicketId'));
        this.router.navigate(['/overview']);
      }
}