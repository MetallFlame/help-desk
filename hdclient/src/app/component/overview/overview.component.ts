import { Component, OnDestroy, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { Ticket } from 'src/app/model/ticket';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SubscriptionLike } from 'rxjs';
import { Feedback } from 'src/app/model/feedback';
import { Attachment } from 'src/app/model/attachment';
import { ActivatedRoute } from '@angular/router';
import { CookieStorage } from 'cookie-storage';
import { Comment } from 'src/app/model/comment';
import { History } from 'src/app/model/history';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit, OnDestroy {
  ticketId: string = "";

  attachmentList: Attachment[] = null;
  uploadAttachmentSubscription: SubscriptionLike;
  attachmentListSubscription: SubscriptionLike;
  deleteAttachmentSubscription: SubscriptionLike;
  downloadAttachmentSubscription: SubscriptionLike;

  selectedFile: File = null;
  fileSelectAlert: string = "";

  overviewedTicket: Ticket;
  commentList: Comment[];
  historyList: History[];
  commentListBackup: Comment[];
  historyListBackup: History[];
  overviewSubscription: SubscriptionLike;
  commentsSubscription: SubscriptionLike;
  createCommentSubscription: SubscriptionLike;
  historySubscription: SubscriptionLike;
  feedbackSubscription: SubscriptionLike;
  displayComments: boolean = true;
  feedback: Feedback;
  commentText: string = '';
  isCommentCorrectFlag: boolean = true;
  isCommentHidden: boolean = false;
  isHistoryHidden: boolean = false;

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    const cookieStorage = new CookieStorage();
    this.route.queryParams.subscribe(params => {
      this.ticketId = params.id;
      console.log(params.id);

      // if (this.ticketId == '') {
      //   this.router.navigate(['/tickets']);
      // }


      if  ((localStorage.getItem('userId') == undefined)&&(cookieStorage.getItem('userId') == undefined)) {
        this.router.navigate(['/login']);
      }
  
      if (localStorage.getItem('userId') == undefined) {
        localStorage.setItem('userId', cookieStorage.getItem('userId'));
        console.log(cookieStorage.getItem('userId'));
      }
  
      if (localStorage.getItem('userRole') == undefined) {
        localStorage.setItem('userRole', cookieStorage.getItem('userRole'));
        console.log(cookieStorage.getItem('userRole'));
      }
  
      if (localStorage.getItem('userlogin') == undefined) {
        localStorage.setItem('userLogin', cookieStorage.getItem('userLogin'));
        console.log(cookieStorage.getItem('userLogin'));
      }

      this.attachmentListRequest(this.ticketId);
      this.ticketOverviewRequest(this.ticketId);
      this.feedbackRequest(this.ticketId);

      this.showComments();
    });
  }

  ngOnDestroy(): void {
  }

  onFileSelected(event) {
    this.fileSelectAlert = "";
    this.selectedFile = <File>event.target.files[0];
    let partsCount = this.selectedFile.name.split('.').length;
    let fileExtension = this.selectedFile.name.split('.')[partsCount - 1];
    console.log(fileExtension);
    if (!["jpg", "jpeg", "png", "doc", "docx", "pdf"].includes(fileExtension)) {
      this.fileSelectAlert = "Incorrect filetype!"
      this.selectedFile = null;
    }
  }

  ticketOverviewRequest(ticketId: String) {
    this.overviewSubscription = this.http.get('http://localhost:8080/api/v1/tickets/' + ticketId).subscribe((data: Ticket) => {
      this.overviewedTicket = data;
      this.overviewedTicket.desiredResolutionDate = this.returnDate(this.overviewedTicket.desiredResolutionDate);
      this.overviewedTicket.createdOn = this.returnDate(this.overviewedTicket.createdOn);
      this.overviewSubscription.unsubscribe();
    });
  }

  ticketCommentsRequest(ticketId: String) {
    this.commentsSubscription = this.http.get('http://localhost:8080/api/v1/comments?ticketId=' + ticketId).subscribe((data: Comment[]) => {
      this.commentList = data;
      this.commentList.forEach(comment => comment.date = this.returnDate(comment.date));
      this.commentListBackup = this.commentList;
      console.log(this.commentList);
      this.commentList.sort((a, b) =>  a['date'] < b['date'] ? 1 : a['date'] > b['date'] ? -1 : 0);
      if (this.commentList.length > 5) {
        this.commentList = this.commentList.slice(0, 5);
        this.isCommentHidden = true;
      }
      this.commentsSubscription.unsubscribe();
    });
  }


  ticketHistoryRequest(ticketId: string) {
    this.historySubscription = this.http.get('http://localhost:8080/api/v1/histories?ticketId=' + ticketId).subscribe((data: History[]) => {
      this.historyList = data;
      this.historyList.forEach(history => history.date = this.returnDate(history.date));
      this.historyListBackup = data;
      this.historyList.sort((a, b) =>  a['date'] < b['date'] ? 1 : a['date'] > b['date'] ? -1 : 0);
      if (this.historyList.length > 5) {
        this.historyList = this.historyList.slice(0, 5);
        this.isHistoryHidden = true;
      }
      this.historySubscription.unsubscribe();
    });
  }

  editTicket() {
    localStorage.setItem('editedTicketId', this.overviewedTicket.id.toString());
    this.router.navigate(['/edit']);
  }

  goToTickets() {
    this.router.navigate(['/tickets']);
  }

  showComments() {
    this.ticketCommentsRequest(this.ticketId);
    this.displayComments = true;
  }

  showHistory() {
    this.ticketHistoryRequest(this.ticketId);
    this.displayComments = false;
  }

  createComment() {
    this.commentCreateRequest(this.overviewedTicket.id.toString());
  }

  leaveFeedback() {
    localStorage.setItem('feedbackTicketId', this.overviewedTicket.id.toString())
    this.router.navigate(['/feedback']);
  }

  feedbackRequest(ticketId: String): void {
    this.feedbackSubscription = this.http.get('http://localhost:8080/api/v1/feedbacks?ticketId=' + ticketId).subscribe((data: Feedback) => {
      this.feedback = data;
      this.feedbackSubscription.unsubscribe();
    })

  }

  commentCreateRequest(ticketId: String) { 
    if (this.isCommentCorrectFlag&&(this.commentText!='')) {
    const request = {text: this.commentText, userId: localStorage.getItem('userId')};
    this.createCommentSubscription = this.http.post('http://localhost:8080/api/v1/comments?ticketId=' + ticketId, request).subscribe((data: any) => {
        this.createCommentSubscription.unsubscribe();
        this.showComments();
        this.commentText = '';
      });
    }
  }

  showButtonForLeaveFeedback(): boolean {
    if ((this.feedback == null)&&(this.overviewedTicket.state == "DONE")&&(localStorage.getItem('userRole') == "EMPLOYEE")) {
      return true;
    }
    return false;
  }


  showButtonForAddAttachment(): boolean {
    if (((localStorage.getItem('userRole') == "EMPLOYEE")||(localStorage.getItem('userRole') == "MANAGER"))&&(this.overviewedTicket.state != "DONE")) {
      return true;
    }
      return false;
  }

  goToAddAttachment() {
    localStorage.setItem('ticketForAttachId', this.overviewedTicket.id.toString());
    this.router.navigate(['/attachment']);
  }

  attachmentListRequest(ticketId: string) {
    this.attachmentListSubscription = this.http.get('http://localhost:8080/api/v1/attachments?ticketId=' + ticketId).subscribe((data: Attachment[]) => {
      this.attachmentList = data;
      this.attachmentListSubscription.unsubscribe();
    });
  }

  deleteAttachment(attachmentId: number): void {
    this.deleteAttachmentSubscription = this.http.delete('http://localhost:8080/api/v1/attachments/' + attachmentId.toString() + '?ticketId=' + this.overviewedTicket.id.toString() + '&userId=' + localStorage.getItem('userId')).subscribe((data: any) => {
      this.deleteAttachmentSubscription.unsubscribe();
      this.attachmentListRequest(this.overviewedTicket.id.toString());
    });
  }

  downloadAttachment(attachmentId: string,  filename: string): void {
    this.downloadAttachmentSubscription = this.http.get('http://localhost:8080/api/v1/attachments/' + attachmentId,{responseType: 'blob' as 'json'}).subscribe(
        (response: any) =>{
            const blob = new Blob([response], { type: 'application/octet-stream' });
            let downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(blob);
            document.body.appendChild(downloadLink);
            downloadLink.setAttribute('download', filename);
            downloadLink.click();
        }
    )
  }

  onUpload() {
    const myHeaders = new HttpHeaders().set('Content-Type', 'multipart/form-data');

    const fd = new FormData();

    const request = {name: this.selectedFile.name, content: this.selectedFile};

    fd.append('attachment', this.selectedFile, this.selectedFile.name);

    this.uploadAttachmentSubscription = this.http.post('http://localhost:8080/api/v1/attachments?ticketId=' +  this.overviewedTicket.id.toString() + '&userId=' + localStorage.getItem('userId'), fd, {
      headers: {
        'enctype': 'multipart/form-data; boundary=request-boundary',

      }
    })
        .subscribe(() => {
            this.uploadAttachmentSubscription.unsubscribe();
            this.attachmentListRequest(this.overviewedTicket.id.toString());
            this.selectedFile = null;
        });
  }

  regexpCheck(stringForCheck: string): boolean {
    let regexp: RegExp = /[^0-9a-zA-Z\~\.\s\"\(\)\,\:\;\<\>\@\[\]\!\#\$\%\&\'\*\+-\/\=\?\^\_\`\{\|\}]/;
    if (regexp.test(stringForCheck)) {
    return false;
    }
    return true;
  }

  stateNormalize(state: string): string {
    state = state.toLowerCase();
    return  (state[0].toLocaleUpperCase() + state.substr(1)).replace("_", " ");
  }

  onCommentChange() {
    if((this.commentText == '')||(this.regexpCheck(this.commentText)&&this.commentText.length < 500)) {
      this.isCommentCorrectFlag = true;
    } else {
      this.isCommentCorrectFlag = false;
    }
  }

  showAllComments() {
    this.commentList = this.commentListBackup;
    this.isCommentHidden = false;
  }

  showAllHistory() {
    this.historyList = this.historyListBackup;
    this.isHistoryHidden = false;
  }

  returnDate(localDate: any): Date {
    var date = new Date();

    date.setFullYear(localDate.year);
    date.setMonth(localDate.monthValue);
    date.setDate(localDate.dayOfMonth);
    date.setHours(localDate.hour);
    date.setMinutes(localDate.minute);
    date.setSeconds(localDate.second);
    return date;
  }

}
