import { Component, OnDestroy, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { SubscriptionLike } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit, OnDestroy {

  feedbackTicketId: String;
  feedbackText: String;
  feedbackRate: number;
  feedbackSubscription: SubscriptionLike;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    if  (localStorage.getItem('userId') == undefined) {
      this.router.navigate(['/login']);
    }

    if (localStorage.getItem('feedbackTicketId') == undefined) {
      this.router.navigate(['/tickets']);
    }

    this.feedbackTicketId = localStorage.getItem('feedbackTicketId');
  }

  ngOnDestroy(): void {
    localStorage.removeItem('feedbackTicketId');
  }

  createFeedback(): void {
    const request = {text: this.feedbackText, rate: this.feedbackRate};

    this.feedbackSubscription = this.http.post('http://localhost:8080/api/v1/feedbacks/' + this.feedbackTicketId + '/' +  localStorage.getItem('userId'), request).subscribe((data: number) => {
      if (data == 1) {
        console.log('connection test passed');
        this.goToOverview();
      }
      this.feedbackSubscription.unsubscribe();
      
    })
    
  }

  goToOverview() {
    this.router.navigate(['/overview'], { queryParams: { id: this.feedbackTicketId.toString() }, queryParamsHandling: 'merge' });
  }

}
