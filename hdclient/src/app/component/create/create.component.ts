import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { Ticket } from 'src/app/model/ticket';
import { Subscription, SubscriptionLike } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Urgency } from 'src/app/model/urgency';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  createForm: any = {
    name: '',
    description: '',
    category: '',
    urgency: '',
    desiredResolutionDate: ''
  }

  isNameCorrectFlag: boolean = true;
  isDescriptionCorrectFlag: boolean = true;
  isCommentCorrectFlag: boolean = true;
  createInputWarningFlag: boolean = false;
  createSelectWarningFlag: boolean = false;
  createCommentWarningFlag: boolean = false;

  dateMin = Date.now();

  urgency = Urgency;

  categoryList: String[];

  ticketSubscription: SubscriptionLike;
  categorySubscription: SubscriptionLike;
  createCommentSubscription: SubscriptionLike;

  commentText: string = '';

  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit(): void {
    if  (localStorage.getItem('userId') == undefined) {
      this.router.navigate(['/login']);
    }

      this.getCategories(); 

  }

  getCategories() {
    this.categorySubscription = this.http.get('http://localhost:8080/api/v1/categories').subscribe((data: String[]) => {
      this.categoryList = data;
      this.categorySubscription.unsubscribe();
    });
  }

  createTicket(status: string) {
    if (this.isNameCorrect()&&
    this.isDescriptionCorrect()&&
    this.isCategorySelected()&&
    this.isUrgencySelected()&&
    this.isNameinputted()&&
    this.isCommentCorrect()) {
      status = status.toUpperCase();
     var  desiredDate: String =  this.createForm.desiredResolutionDate;
    const request = {name: this.createForm.name, 
      description: this.createForm.description,
      category: this.createForm.category, 
      state: status,
      urgency: this.createForm.urgency.toUpperCase( )};
      
      this.ticketSubscription = this.http.post('http://localhost:8080/api/v1/tickets?userId=' + localStorage.getItem('userId') + '&desiredDate=' + desiredDate.substring(0, 4) +'-' + desiredDate.substring(5, 7) + '-' + desiredDate.substring(8), request).subscribe((data: number) => {
        
         this.ticketSubscription.unsubscribe();
         
         if ((this.commentText != '')&&(data != 0)) {
            this.commentCreateRequest(data.toString());
         }
        
        this.goToTickets();
       });
      } 
      if (!this.isDescriptionCorrect()||!this.isNameCorrect()) {
        this.createInputWarningFlag = true;
      }

      if (!this.isNameinputted()||!this.isUrgencySelected()||!this.isCategorySelected()) {
        this.createSelectWarningFlag = true;
      }

      if (!this.isCommentCorrect()) {
        this.createCommentWarningFlag = true;
      }

  }

  
  commentCreateRequest(ticketId: String) {
    const request = {text: this.commentText, userId: localStorage.getItem('userId')};
    this.createCommentSubscription = this.http.post('http://localhost:8080/api/v1/comments?ticketId=' + ticketId, request).subscribe((data: number) => {
        this.createCommentSubscription.unsubscribe();

      });
  }

  goToTickets() {
    this.router.navigate(['/tickets']);
  }

  regexpCheck(stringForCheck: string): boolean {
    let regexp: RegExp = /[^0-9a-zA-Z\~\.\s\"\(\)\,\:\;\<\>\@\[\]\!\#\$\%\&\'\*\+-\/\=\?\^\_\`\{\|\}]/;
    if (regexp.test(stringForCheck)) {
    return false;
    }
    return true;
  }

  isNameCorrect(): boolean {
    if (this.regexpCheck(this.createForm.name)&&this.createForm.name.length < 100) {
      return true;
    }
    return false;
  }

  isDescriptionCorrect(): boolean {
    if (this.regexpCheck(this.createForm.description)&&this.createForm.description.length < 500) {
      return true;
    }
    return false;
  }

  isCategorySelected(): boolean {
    if (this.createForm.category != '') { 
      this.createSelectWarningFlag = false;
      return true;
    }
    return false;
  }

  isUrgencySelected(): boolean {
    if (this.createForm.urgency != '') {
      this.createSelectWarningFlag = false;
      return true;
    }
    return false;
  }

  isNameinputted(): boolean {
    if (this.createForm.name != '') {
      this.createSelectWarningFlag = false;
      return true;
    }
    return false;
  }

  isCommentCorrect(): boolean {
    if (this.regexpCheck(this.commentText)&&this.commentText.length < 500) {
      return true;
    }
    return false;
  }

  onNameChange() {
    this.createInputWarningFlag = false;
    if (this.isNameCorrect()) {
      this.isNameCorrectFlag = true;
    } else {
      this.isNameCorrectFlag = false;
    }
  }

  onDescriptionChange() {
    this.createInputWarningFlag = false;
    if (this.isDescriptionCorrect()) {
      this.isDescriptionCorrectFlag = true;
    } else {
      this.isDescriptionCorrectFlag = false;
    }
  }

  onCommentChange() {
    this.createCommentWarningFlag = false;
    if (this.isCommentCorrect()) {
      this.isCommentCorrectFlag = true;
    } else {
      this.isCommentCorrectFlag = false;
    }

  }
  

}


