import { Component, OnInit } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'hdclient';

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    localStorage.clear();
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);

  }
}
